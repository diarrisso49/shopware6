<?php declare(strict_types=1);

namespace DemoShop\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Log\Package;
use Shopware\Core\Framework\Migration\MigrationStep;

/**
 * @internal
 */
#[Package('core')]
class Migration1717056126 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1717056126;
    }

    /**
     * @throws Exception
     */
    public function update(Connection $connection): void
    {
        $connection->executeStatement('
            CREATE TABLE `shop_finder` (
                `id`        BINARY(16)   NOT NULL,
                `active`    TINYINT(1)   NOT NULL DEFAULT 0,
                `name`      VARCHAR(255) NOT NULL,
                `street`    VARCHAR(255) NOT NULL,
                `post_code`  VARCHAR(255) NOT NULL ,
                `city`      VARCHAR(255) NOT NULL,
                `url`       VARCHAR(255) NULL,
                `telephone` VARCHAR(255) NULL,
                `open_times` LONGTEXT    NULL,
                `country_id` BINARY(16)  NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3),
                PRIMARY KEY (`id`),
                KEY `fk.shop_finder.country_id` (`country_id`),
                CONSTRAINT `fk.shop_finder.country_id` FOREIGN KEY (`country_id`)
                    REFERENCES `country` (`id`) ON DELETE SET NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive( Connection $connection) : void
    {

    }
}
