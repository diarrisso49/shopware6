<?php declare(strict_types=1);


namespace Shopware\Custom\Plugins\DemoShop\Resources\snippet\en_GB;




use Shopware\Core\System\Snippet\Files\AbstractSnippetFile;

class SnippetFile_deDE extends AbstractSnippetFile
{


    public function getName(): string
    {
        return 'storefront.en_GB';
    }

    public function getPath(): string
    {
        return __DIR__ . '/storefront.de_DE.json';
    }

    public function getIso(): string
    {
        return 'en_GB';
    }

    public function getAuthor(): string
    {
        return 'DemoShop';
    }

    public function isBase(): bool
    {
        return false;
    }

    public function getTechnicalName(): string
    {
        return 'DemoShop';
    }
}