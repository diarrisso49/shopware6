<?php declare(strict_types=1);


namespace DemoShop\Resources\snippet\en_GB;


use Shopware\Core\System\Snippet\Files\AbstractSnippetFile;

class SnippetFile_en_GB extends AbstractSnippetFile
{


    public function getName(): string
    {
        return 'storefront.en_GB';
    }

    public function getPath(): string
    {
        return __DIR__.'/storefront.en_GB.json';
    }

    public function getIso(): string
    {
        return 'en_GB';
    }

    public function getAuthor(): string
    {
        return 'DemoShop';
    }

    public function isBase(): bool
    {
        return false;
    }

    public function getTechnicalName(): string
    {
        return 'DemoShop';
    }
}