<?php

namespace DemoShop\Subscriber;

use DemoShop\Core\Content\ShopFinder\ShopFinderCollection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Pagelet\Footer\FooterPageletLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

readonly class FooterSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private SystemConfigService $systemConfigService,
        private EntityRepository  $shopFinderRepository)
    {}

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FooterPageletLoadedEvent::class => 'onFooterPageletLoaded',
        ];
    }

    public function onFooterPageletLoaded(FooterPageletLoadedEvent $event): void
    {
        if (!$this->systemConfigService->get('DemoShop.config.showInStorefront')) {
            return;
        }

        $shops = $this->fetchShop($event->getContext());
        $event->getPagelet()->addExtension('shop_finder', $shops);
    }

    private function fetchShop(Context $context): ShopFinderCollection
    {
        $criteria = new Criteria();
        $criteria->addAssociation('country');
        $criteria->addFilter(new EqualsFilter('active', '1'));
        $criteria->setLimit(4);

        /**
         * @var ShopFinderCollection $shopFinderCollection
         */

        $shopFinderCollection = $this->shopFinderRepository->search($criteria, $context)->getEntities();

        return $shopFinderCollection;
    }



}