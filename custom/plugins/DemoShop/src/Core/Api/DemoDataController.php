<?php

namespace DemoShop\Core\Api;

use Faker\Factory;

use RuntimeException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\Country\CountryEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


#[Route(defaults: ['_routeScope' => ['api']])]
class DemoDataController extends AbstractController
{
    public function __construct(
        private readonly EntityRepository $shopFinderRepository,
        private readonly EntityRepository $countryRepository
    ){}

    #[Route(
        path: '/api/v{version}/_action/demo-shop-finder/generate',
        name: 'api.demo.data',
        methods: ['POST']
    )]
    public function generate(Context $context ): Response
    {
        $faker = Factory::create('de_DE');
        $country = $this->getActiveCountry($context);



        $data = [];

        for ($i = 1; $i < 5; $i++) {


            $data[] = [
                'id' => Uuid::randomHex(),
                'active' => true,
                'name' => $faker->name(),
                'street' => $faker->streetAddress(),
                'postCode' =>  $faker->postcode(),
                'city' => $faker->city(),
                'countryId' => $country->getId(),
            ];
        }


        $this->shopFinderRepository->create($data, $context);

        return new Response('dada has been successfully created', Response::HTTP_OK);
    }

    /**
     * @param Context $context
     * @return CountryEntity
     */
    private function getActiveCountry(Context $context) : CountryEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('active', '1'));
        $criteria->setLimit(1);
        /** @var CountryEntity|null $country */
        $country = $this->countryRepository->search($criteria, $context)->getEntities()->first();

        if ($country === null) {

            throw new RuntimeException('No active country');
        }

        return $country;
    }

}