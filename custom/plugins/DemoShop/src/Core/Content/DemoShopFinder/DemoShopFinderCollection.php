<?php declare(strict_types=1);

namespace DemoShop\Core\Content\DemoShopFinder;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void add(DemoShopFinderEntity $entity)
 * @method void set(string $key, DemoShopFinderEntity $entity)
 * @method DemoShopFinderEntity[] getIterator()
 * @method DemoShopFinderEntity[] getElements()
 * @method DemoShopFinderEntity|null get(string $key)
 * @method DemoShopFinderEntity|null first()
 * @method DemoShopFinderEntity|null last()
 */
class DemoShopFinderCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return DemoShopFinderEntity::class;
    }
}
