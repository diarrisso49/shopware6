<?php declare(strict_types=1);

namespace DemoShop\Core\Content\Kunden;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void add(KundenEntity $entity)
 * @method void set(string $key, KundenEntity $entity)
 * @method KundenEntity[] getIterator()
 * @method KundenEntity[] getElements()
 * @method KundenEntity|null get(string $key)
 * @method KundenEntity|null first()
 * @method KundenEntity|null last()
 */
class KundenCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return KundenEntity::class;
    }
}
