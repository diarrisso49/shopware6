<?php

namespace DemoShop\Core\Content\ShopFinder;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class ShopFinderCollection extends EntityCollection
{

    public function getExpectedClass(): string
    {
        return ShopFinderEntity::class;

    }

}