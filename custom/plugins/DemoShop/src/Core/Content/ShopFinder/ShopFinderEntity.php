<?php

namespace DemoShop\Core\Content\ShopFinder;

use League\Event\EmitterTrait;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\System\Country\CountryEntity;

/**
 *
 */
class ShopFinderEntity extends Entity
{


    use EmitterTrait;

    /**
     * @var bool
     */
    protected bool $active;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var string
     */
    protected string $street;

    /**
     * @var string
     */
    protected string $postCode;


    /**
     * @var string|null
     */
    protected ?string $city;


    /**
     * @var string|null
     */
    protected  ?string $url;

    /**
     * @var string|null
     */
    protected ?string $telephone;

    /**
     * @var string|null
     */
    protected ?string $openTimes;


    /**
     * @var CountryEntity|null
     */
    protected ?CountryEntity $country;


    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getPostCode(): string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): void
    {

        $this->postCode = $postCode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    public function getOpenTimes(): ?string
    {
        return $this->openTimes;
    }

    public function setOpenTimes(?string $openTimes): void
    {
        $this->openTimes = $openTimes;
    }

    public function getCountry(): ?CountryEntity
    {
        return $this->country;
    }

    public function setCountry(?CountryEntity $country): void
    {
        $this->country = $country;
    }



}